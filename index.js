module.exports.recursivePromise = async (arr) => {
    let resultsArr = []
    await arr.forEach((promise) => {
        promise
            .then(res => resultsArr.push(res))
            .catch(err => resultsArr.push(new Error(err)))
    });
    return resultsArr
}