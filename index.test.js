const { recursivePromise } = require("./index")
const strPromise = new Promise((resolve, reject) => {
    resolve("lorem ipsum")
})
const numPromise = new Promise((resolve, reject) => {
    resolve(5)
})
const errPromise = new Promise((resolve, reject) => {
    reject("error")
})
const promisesArr = [strPromise, numPromise, errPromise, strPromise, numPromise, numPromise, strPromise, errPromise, errPromise, errPromise]
const tests = () => {
    test("check containg string", async () => {
        expect(await recursivePromise(promisesArr)).toContain("lorem ipsum");
    });
    test("chceck containg number", async () => {
        expect(await recursivePromise(promisesArr)).toContain(5);
    });
    test("check if array length is same as promisses array length", async () => {
        console.log(await recursivePromise(promisesArr))
        expect(await recursivePromise(promisesArr)).toHaveLength(promisesArr.length)
    })
}
tests()